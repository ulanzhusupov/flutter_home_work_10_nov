import 'package:flutter/material.dart';
import 'package:flutter_home_work_10_nov/theme/app_fonts.dart';

class InputWidget extends StatelessWidget {
  const InputWidget(
      {super.key,
      required this.controller,
      required this.hintText,
      required this.labelText,
      this.keyboardType = TextInputType.text,
      this.obscureText = false});
  final String hintText;
  final String labelText;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final bool obscureText;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          labelText,
          style: AppFonts.s16W400,
          textAlign: TextAlign.start,
        ),
        TextFormField(
          obscureText: obscureText,
          cursorColor: const Color.fromRGBO(255, 255, 255, 0.9),
          keyboardType: keyboardType,
          autofocus: false,
          textAlign: TextAlign.start,
          textInputAction: TextInputAction.done,
          controller: controller,
          style: AppFonts.s16W400.copyWith(color: Colors.white),
          decoration: InputDecoration(
            floatingLabelBehavior: FloatingLabelBehavior.auto,
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15.0),
              borderSide: const BorderSide(
                color: Color.fromRGBO(255, 255, 255, 0.898),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15.0),
              borderSide: const BorderSide(
                color: Color.fromRGBO(255, 255, 255, 0.898),
              ),
            ),
            contentPadding:
                const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
            hintText: hintText,
            hintStyle: AppFonts.s16W400
                .copyWith(color: const Color.fromRGBO(255, 255, 255, 0.5)),
          ),
        ),
      ],
    );
  }
}
