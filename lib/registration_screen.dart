import 'package:flutter/material.dart';
import 'package:flutter_home_work_10_nov/theme/app_fonts.dart';
import 'package:flutter_home_work_10_nov/widgets/input_widget.dart';

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({super.key});

  @override
  State<RegistrationScreen> createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  TextEditingController surnameController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(colors: [
          Color(0xFF8C4AE2),
          Color.fromARGB(255, 62, 28, 116),
        ], begin: Alignment.topRight, end: Alignment.bottomCenter),
      ),
      child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios_new_rounded,
                  color: Colors.white),
              onPressed: () => Navigator.of(context).pop(),
            ),
            centerTitle: true,
            title: Text(
              "Регистрация",
              style: AppFonts.s20W600.copyWith(color: Colors.white),
            ),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  InputWidget(
                    controller: surnameController,
                    hintText: "Введите Фамилию",
                    labelText: "Фамилия",
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  InputWidget(
                    controller: nameController,
                    hintText: "Введите Имя",
                    labelText: "Имя",
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  InputWidget(
                    controller: phoneController,
                    hintText: "(+996) 555 000 000",
                    labelText: "Номер телефона",
                    keyboardType: TextInputType.phone,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  InputWidget(
                    controller: passwordController,
                    hintText: "Введите пароль",
                    labelText: "Пароль",
                    keyboardType: TextInputType.visiblePassword,
                    obscureText: true,
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: ElevatedButton(
                      onPressed: () {},
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Text(
                          "Зарегистрироваться",
                          style: AppFonts.s18W700
                              .copyWith(color: Color(0xFF8C4AE2)),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          )),
    );
  }
}
