import 'package:flutter/material.dart';

abstract class AppFonts {
  static const TextStyle s16W400 =
      TextStyle(fontSize: 16, fontWeight: FontWeight.w400, color: Colors.white);

  static const TextStyle s12W700 =
      TextStyle(fontSize: 12, fontWeight: FontWeight.w700, color: Colors.white);

  static const TextStyle s18W700 =
      TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white);

  static const TextStyle s20W600 =
      TextStyle(fontSize: 20, fontWeight: FontWeight.w600, color: Colors.white);

  static const TextStyle s30W700 =
      TextStyle(fontSize: 30, fontWeight: FontWeight.w700, color: Colors.white);
}
