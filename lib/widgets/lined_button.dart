import 'package:flutter/material.dart';

class LinedButton extends StatelessWidget {
  const LinedButton(
      {super.key,
      required this.buttonText,
      required this.padVertical,
      required this.padHorizontal,
      required this.textStyle,
      required this.onPressed});
  final String buttonText;
  final double padVertical;
  final double padHorizontal;
  final TextStyle textStyle;
  final Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: onPressed,
      style: OutlinedButton.styleFrom(
        padding: EdgeInsets.symmetric(
            vertical: padVertical, horizontal: padHorizontal),
        side: const BorderSide(width: 1.5, color: Colors.white),
      ),
      child: Text(
        buttonText,
        style: textStyle,
      ),
    );
  }
}
