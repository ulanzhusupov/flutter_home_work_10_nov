import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_home_work_10_nov/registration_screen.dart';
import 'package:flutter_home_work_10_nov/theme/app_fonts.dart';
import 'package:flutter_home_work_10_nov/widgets/lined_button.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      debugShowCheckedModeBanner: false,
      home: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(colors: [
            Color(0xFF8C4AE2),
            Color.fromARGB(255, 62, 28, 116),
          ], begin: Alignment.topRight, end: Alignment.bottomCenter),
        ),
        child: WelcomeScreen(),
      ),
    );
  }
}

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 20),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(
                height: 150,
              ),
              Container(
                  // color: Colors.white,
                  width: 117,
                  height: 99,
                  child: Image.asset("assets/images/ezy_logo.png")),
              const Expanded(
                child: Column(
                  children: [
                    SizedBox(
                      height: 91,
                    ),
                    Text("Добро пожаловать!", style: AppFonts.s30W700),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Приветствуем вас на площадке аренды строительной техники",
                      style: AppFonts.s16W400,
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    LinedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const RegistrationScreen()),
                        );
                      },
                      buttonText: "Регистрация",
                      padVertical: 19,
                      padHorizontal: MediaQuery.of(context).size.width * 0.27,
                      textStyle: AppFonts.s18W700,
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    TextButton(
                      onPressed: () {},
                      child: const Text(
                        "У меня уже есть аккаунт",
                        style: AppFonts.s16W400,
                      ),
                    ),
                    const SizedBox(
                      height: 77,
                    ),
                  ],
                ),
              ),
              LinedButton(
                buttonText: "Магазин автозапчастей",
                textStyle: AppFonts.s12W700,
                padVertical: 10,
                padHorizontal: MediaQuery.of(context).size.width * 0.07,
                onPressed: () {},
              ),
              const SizedBox(
                height: 28,
              )
            ],
          ),
        ),
      ),
    );
  }
}
